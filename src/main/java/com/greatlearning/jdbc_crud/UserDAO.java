package com.greatlearning.jdbc_crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class UserDAO {

	public static void create(Connection conn, User user) throws SQLException {
		if(conn !=null && !conn.isClosed()) {
			String query =  "insert into users (userId, firstName, lastName, emailId) values (?,?,?,?)";
        	conn.setAutoCommit(false);		
			Savepoint savepoint1 = null;
			try {
				PreparedStatement pstm = conn.prepareStatement(query);
				pstm.setInt(1, user.getUserId());
				pstm.setString(2, user.getFirstName());
				pstm.setString(3, user.getLastName());
				pstm.setString(4, user.getEmailid());
				
				savepoint1 = conn.setSavepoint("Savepoint1");
				System.out.println("Savepoint 1");
				int row = pstm.executeUpdate();
				
				conn.commit();
				pstm.close();
				System.out.println("Successfully registered the user");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				conn.rollback();
				System.out.println("Rollback 1");
				e.printStackTrace();
			}
		}
		
	}

	public static void update(Connection conn, User user) throws SQLException {
		if(conn !=null && !conn.isClosed()) {
		String query =  "update users set userId= ?, firstName= ?, lastName= ?, emailId= ? where userId=?";
    	conn.setAutoCommit(false);	
		Savepoint savepoint2 = null;
		try {
				PreparedStatement pstm = conn.prepareStatement(query);
				pstm.setInt(1, user.getUserId());
				pstm.setString(2, user.getFirstName());
				pstm.setString(3, user.getLastName());
				pstm.setString(4, user.getEmailid());
				pstm.setInt(5, user.getUserId());
				
				savepoint2 = conn.setSavepoint("Savepoint2");
				System.out.println("Savepoint 2");
				int row = pstm.executeUpdate();
				conn.commit();
				pstm.close();
				System.out.println("Successfully updated the user");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("Ensure that the user exists ! ");
				conn.rollback();
				System.out.println("Rollback 2");
				e.printStackTrace();
			}
		}

	}

	public static void display(Statement stm) {
		String query =  "select userId, firstName, lastName, emailId from users";
		ResultSet rs;
		try {
			rs = stm.executeQuery(query);
			while(rs.next()) {
				int userId = rs.getInt("userId");
				String firstName = rs.getString("firstName");
				String lastName = rs.getString("lastName");
				String emailId = rs.getString("emailId");
				System.out.println("userId: "+ userId +", firstName: "+firstName + ", lastName: "+ lastName+ ", emailId: "+emailId);
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		
	public static void delete(Connection conn, int userId) throws SQLException {
		
		if(conn !=null && !conn.isClosed()) {
		String query =  "delete from users where userId=?";
    	conn.setAutoCommit(false);
		Savepoint savepoint3 = null;
			try {
				
				PreparedStatement pstm = conn.prepareStatement(query);
				pstm.setInt(1, userId);
				savepoint3 = conn.setSavepoint("Savepint3");
				System.out.println("Savepoint 3");
				int row = pstm.executeUpdate();			
				conn.commit();
				pstm.close();
				System.out.println("Successfully deleted the user");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("Ensure that the userId exists");
				conn.rollback();
				System.out.println("Rollback 3");
				e.printStackTrace();
			}
		}
	}
}
