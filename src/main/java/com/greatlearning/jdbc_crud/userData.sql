create database if not exists userdata;

use userdata;
drop table if exists users;

CREATE TABLE `users` (
   `userId` int NOT NULL,
   `firstName` varchar(45) NOT NULL,
   `lastName` varchar(45) NOT NULL,
   `emailId` varchar(45) NOT NULL,
   PRIMARY KEY (`userId`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

insert into users values(101,'Shifa', 'Samreen', 'shifa@gmail.com');
insert into users values(102,'John', 'Doe', 'john@gmail.com');
insert into users values(103,'Scarlet', 'Witch', 'scar@gmail.com');

select * from users;
delete from userdata.users where id=103;
truncate table userdata.users;