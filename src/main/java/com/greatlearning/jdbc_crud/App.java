package com.greatlearning.jdbc_crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException
    {
    	Savepoint savepoint = null;
        try {
        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdata", "root", "@RootpassworD");       	
        	Statement stm = conn.createStatement();
        	conn.setAutoCommit(false);
        	while(true) {
		    	System.out.println("\n");
		    	System.out.println("Welcome to the user CRUD services !");
		    	System.out.println("\n");
		    	System.out.println("Enter the operation you want to perform-");
		    	System.out.println("-------------------------------");
				System.out.println("1. Registration");
				System.out.println("2. Update");
				System.out.println("3. Display");
				System.out.println("4. Delete");
				System.out.println("5. Exit");
		    	System.out.println("-------------------------------");
		    	System.out.println("\n");
	
				Scanner scanner = new Scanner(System.in);
		    	int option = scanner.nextInt();
		    	
		    	UserDAO userDao = new UserDAO();
		    	
		    	switch(option) {
		    	case 1:
			    	System.out.println("-------------------------------");
					System.out.println("Enter user details");
			    	System.out.println("-------------------------------");
					System.out.println("UserId: ");
		    		int userIdCreate = scanner.nextInt();
					System.out.println("First Name: ");
		    		String firstNameCreate = scanner.next();
					System.out.println("Last Name: ");
		    		String lastNameCreate = scanner.next();
					System.out.println("Email Id: ");
		    		String emailIdCreate = scanner.next();
		    		User userCreate = new User(userIdCreate, firstNameCreate,lastNameCreate,emailIdCreate );
		    		System.out.println("User details entered: ");
					System.out.println("userId: "+ userIdCreate +", firstName: "+firstNameCreate + ", lastName: "+ lastNameCreate+ ", emailId: "+emailIdCreate);
		    		UserDAO.create(conn, userCreate);
		    		break;
		    	case 2:
		    		System.out.println("-------------------------------");
					System.out.println("Enter user details");
			    	System.out.println("-------------------------------");
					System.out.println("UserId: ");
		    		int userIdUpdate = scanner.nextInt();
					System.out.println("First Name: ");
		    		String firstNameUpdate = scanner.next();
					System.out.println("Last Name: ");
		    		String lastNameUpdate = scanner.next();
					System.out.println("Email Id: ");
		    		String emailIdUpdate = scanner.next();
		    		User userUpdate = new User(userIdUpdate, firstNameUpdate,lastNameUpdate,emailIdUpdate );
		    		System.out.println("User details entered: ");
					System.out.println("userId: "+ userIdUpdate +", firstName: "+firstNameUpdate + ", lastName: "+ lastNameUpdate+ ", emailId: "+emailIdUpdate);
		    		UserDAO.update(conn, userUpdate);
		    		break;
		    	case 3:
		    		UserDAO.display(stm);
		    		break;
		    	case 4:
					System.out.println("Enter userId to delete: ");
		    		int userIdDelete = scanner.nextInt();
		    		UserDAO.delete(conn, userIdDelete);
		    		break;
		    	case 5:
		    		System.out.println("Exit succesful ! ");
		    		scanner.close();
		    		stm.close();
		    		return;
		    	default:
		    		System.out.println("Please enter a valid integer option! ");
		    		break;
		    	}
        	}
        } catch(Exception ex) {
        	ex.printStackTrace();
        }
    }
}
